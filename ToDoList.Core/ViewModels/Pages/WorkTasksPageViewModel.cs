﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Services;
using System.Threading.Tasks;
using ToDoList.Entities;

namespace ToDoList.Core
{
    public class WorkTasksPageViewModel : BaseViewModel
    {
        public ObservableCollection<WorkTaskViewModel> WorkTaskList { get; set; } = new ObservableCollection<WorkTaskViewModel>();
        public string NewWorkTaskTitle { get; set; }
        public string NewWorkTaskDescription { get; set; }
        public ICommand AddNewTaskCommand { get; set; }
        public ICommand DeleteSelectedTasksCommand { get; set; }

        WorkTaskServiceClient _workTaskService;

        public WorkTasksPageViewModel()
        {
            AddNewTaskCommand = new RelayCommand(AddNewTask);
            DeleteSelectedTasksCommand = new RelayCommand(DeleteSelectedTasks);
            PopulateWorkTaskList();
        }

        private void PopulateWorkTaskList()
        {
            PopulateWorkTaskListAsync();
        }

        private void DeleteSelectedTasks()
        {
            DeleteSelectedTasksAsync();
        }

        private void AddNewTask()
        {
            AddNewTaskAsync();
        }

        private async Task PopulateWorkTaskListAsync()
        {
            _workTaskService = new WorkTaskServiceClient();
            WorkTaskList.Clear();

            foreach (var task in await _workTaskService.GetAllWorkTasksAsync())
            {
                WorkTaskList.Add(new WorkTaskViewModel
                {
                    Id = task.Id,
                    Title = task.Title,
                    Description = task.Description,
                    CreateDate = task.CreateDate
                });
            }
            await _workTaskService.CloseAsync();
        }

        private async Task AddNewTaskAsync()
        {
            _workTaskService = new WorkTaskServiceClient();
            var newTask = new WorkTaskViewModel
            {
                Title = NewWorkTaskTitle,
                Description = NewWorkTaskDescription,
                CreateDate = DateTime.Now
            };

            NewWorkTaskTitle = "";
            NewWorkTaskDescription = "";

            WorkTaskList.Add(newTask);

            var newTaskEntity = new WorkTask()
            {
                Title = newTask.Title,
                Description = newTask.Description,
                CreateDate = newTask.CreateDate
            };

            await _workTaskService.AddWorkTaskAsync(newTaskEntity);
            await _workTaskService.CloseAsync();
        }

        private async Task DeleteSelectedTasksAsync()
        {
            _workTaskService = new WorkTaskServiceClient();
            var selectedTasks = WorkTaskList.Where(x => x.IsSelected).ToList();

            foreach (var task in selectedTasks)
            {
                WorkTaskList.Remove(task);

                await _workTaskService.RemoveWorkTaskByIdAsync(task.Id);
            }
            await _workTaskService.CloseAsync();
        }


    }

}
