﻿namespace ToDoList.DataAccess.Helpers
{
    public static class Globals
    {
        public static ToDoListDbContext Context { get; set; }
    }
}
