﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ToDoList.Entities;

namespace ToDoList.Services
{
    [ServiceContract]
    public interface IWorkTaskService : IDisposable
    {
        [OperationContract]
        IEnumerable<WorkTask> GetAllWorkTasks();

        [OperationContract]
        void RemoveWorkTaskById(int id);

        [OperationContract]
        void AddWorkTask(WorkTask workTask);
    }
}
