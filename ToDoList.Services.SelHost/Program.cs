﻿using System;
using System.Diagnostics;
using System.ServiceModel;

namespace ToDoList.Services.SelHost
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                ServiceHost host = new ServiceHost(typeof(WorkTaskService));
                host.Open();
                Console.WriteLine("Service started. Press any keu to close");
                Console.ReadKey();
                host.Close();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
    }
}
