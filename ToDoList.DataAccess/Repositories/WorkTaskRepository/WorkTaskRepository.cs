﻿using System.Collections.Generic;
using System.Linq;
using ToDoList.Entities;

namespace ToDoList.DataAccess.Repositories.WorkTaskRepository
{
    public class WorkTaskRepository : IWorkTaskRepository
    {
        private readonly ToDoListDbContext _context;

        public WorkTaskRepository()
        {
            _context = new ToDoListDbContext();
            _context.Database.EnsureCreated();
        }

        public void AddWorkTask(WorkTask workTask)
        {
            _context.WorkTasks.Add(workTask);

            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public IEnumerable<WorkTask> GetAllWorkTasks()
        {
            var tasks = _context.WorkTasks.ToList();
            return tasks;
        }

        public void RemoveWorkTaskById(int id)
        {
            var workTask = _context.WorkTasks.FirstOrDefault(x => x.Id == id);

            if (workTask != null)
            {
                _context.WorkTasks.Remove(workTask);

                _context.SaveChanges();
            }

        }
    }
}
