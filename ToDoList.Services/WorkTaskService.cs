﻿using System.Collections.Generic;
using System.ServiceModel;
using ToDoList.DataAccess.Repositories.WorkTaskRepository;
using ToDoList.Entities;

namespace ToDoList.Services
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class WorkTaskService : IWorkTaskService
    {
        IWorkTaskRepository _workTaskRepository;

        public WorkTaskService()
        {
            _workTaskRepository = new WorkTaskRepository();
        }

        public void AddWorkTask(WorkTask workTask)
        {
            _workTaskRepository.AddWorkTask(workTask);
        }

        public void Dispose()
        {
            _workTaskRepository.Dispose();
        }

        public IEnumerable<WorkTask> GetAllWorkTasks()
        {
            return _workTaskRepository.GetAllWorkTasks();
        }

        public void RemoveWorkTaskById(int id)
        {
            _workTaskRepository.RemoveWorkTaskById(id);
        }
    }
}
