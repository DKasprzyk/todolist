﻿using System;
using System.Runtime.Serialization;

namespace ToDoList.Entities
{
    [DataContract]
    public class WorkTask
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public DateTime CreateDate { get; set; }

    }
}
