﻿using System;
using System.Collections.Generic;
using ToDoList.Entities;

namespace ToDoList.DataAccess.Repositories.WorkTaskRepository
{
    public interface IWorkTaskRepository : IDisposable
    {
        IEnumerable<WorkTask> GetAllWorkTasks();
        void RemoveWorkTaskById(int id);
        void AddWorkTask(WorkTask workTask);
    }
}
